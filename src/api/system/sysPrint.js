import request from '@/utils/request'

export function getPrintByReportCode(reportCode) {
  return request({
    url: '/admin-service/api/sysPrintMng/getPrintByReportCode?reportCode=' + reportCode,
    method: 'get'
  })
}

export function sysPrintTemplateUrl() {
  return 'admin-service/api/sysPrintMng/downloadImportTemplate'
}

export function loadDataUrl() {
  return 'admin-service/api/sysPrintMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/sysPrintMng/doDelete'
}

export function submitUrl() {
  return 'admin-service/api/sysPrintMng/doSubmit'
}

export function destoryUrl() {
  return 'admin-service/api/sysPrintMng/doDestory'
}

export function loadHistoryDataUrl() {
  return 'admin-service/api/sysPrintMng/loadHistoryData'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/toAdd',
    method: 'post',
    data
  })
}

export function toChange(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/toChange',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/sysPrintMng/doDelete',
    method: 'post',
    data
  })
}
